"""This is the main entrypoint of this project.
    When executed as
    python main.py input_folder/
    it should perform the neural network inference on all images in the `input_folder/` and print the results.
"""

from argparse import ArgumentParser
import sys
import os
import tensorflow as tf
import numpy as np
from PIL import Image

from sklearn.preprocessing import MinMaxScaler

class TensorflowLiteClassificationModel:
    def __init__(self, model_path, labels, image_size=180):
        self.interpreter = tf.lite.Interpreter(model_path=model_path)
        self.interpreter.allocate_tensors()
        self._input_details = self.interpreter.get_input_details()
        self._output_details = self.interpreter.get_output_details()
        self.labels = labels
        self.image_size=image_size

    def run_from_filepath(self, image_path):
        input_data_type = self._input_details[0]["dtype"]
        image = np.array(Image.open(image_path).resize((self.image_size, self.image_size)), dtype=input_data_type)
        if input_data_type == np.float32:
            image = image / 255.

        if image.shape == (1, 180, 180):
            image = np.stack(image*3, axis=0)
        image = np.reshape(image , (1, 180, 180 , 3))
        return self.run(image)

    def run(self, image):
        """
        args:
          image: a (1, image_size, image_size, 3) np.array

        Returns list of [Label, Probability], of type List<str, float>
        """

        self.interpreter.set_tensor(self._input_details[0]["index"], image)
        self.interpreter.invoke()
        tflite_interpreter_output = self.interpreter.get_tensor(self._output_details[0]["index"])
        #probabilities = np.array(tflite_interpreter_output[0])
        scaler = MinMaxScaler()
        probabilities = scaler.fit_transform(tflite_interpreter_output[0].reshape(-1,1))
        probabilities = probabilities / probabilities.sum()

        # create list of ["label", probability], ordered descending probability
        label_to_probabilities = []
        for i, probability in enumerate(probabilities):
            label_to_probabilities.append([self.labels[i], float(probability)])
        return sorted(label_to_probabilities, key=lambda element: element[1])

class_names = ['daisy', 'dandelion', 'roses', 'sunflowers', 'tulips']



def parse_args():
    """Define CLI arguments and return them.

    Feel free to modify this function if needed.

    Returns:
        Namespace: parser arguments
    """
    parser = ArgumentParser()
    parser.add_argument("input_folder", type=str, help="A folder with images to analyze.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    cli_args = parse_args()
    # do stuff....
    model = TensorflowLiteClassificationModel("./model.tflite",class_names)
    print(f"Analyzing folder : {cli_args.input_folder}", file=sys.stderr)  # info print must be done on stderr like this for messages
    for filename in os.listdir(cli_args.input_folder):
        path = cli_args.input_folder + filename
        prediction = model.run_from_filepath(path)
        print({filename:{"score":np.round(prediction[-1][1],2), "class": prediction[-1][0]}})
    
